<?php

/**
 * Front to music site application. This file is the start of the application. It only
 * serves to load the rest of the config.
 * 
 * @package Music Site
 * 
 */
    
require_once __DIR__ . '/ms-config.php';
