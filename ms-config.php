<?php

/**
 * The base configuration for the Music Site application. 
 * 
 * MySQL settings
 * Secret Keys
 * Important Constants
 * 
 * @package Music Site
 * 
 */

 /** MySQL settings - Needed for the DB connection */

/** The database name */
define( 'DB_NAME', getenv( 'MS_DB_NAME' ) );

/** The database user */
define( 'DB_USER', getenv( 'MS_DB_USER' ) );

/** The database user password */
define( 'DB_PASS', getenv( 'MS_DB_PASS' ) );

/** The database host name */
define( 'DB_HOST', getenv( 'MS_DB_HOST' ) );

/** The database charset */
define( 'DB_CHARSET', getenv( 'MS_DB_CHARSET' ) );

/** Stores the main dir for all functions, classes, and core logic */
define( 'MSINC', 'ms-includes' );

/** Absolute path to the Music Site app dircetory */
if( !defined('MSPATH' ) ) 
    define( 'MSPATH', __DIR__ . '/' );

require_once MSPATH . MSINC . '/ms-settings.php';





