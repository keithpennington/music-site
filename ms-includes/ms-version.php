<?php

/**
 * Music Site Version
 * 
 * Contains version info for the current release of Music Site
 * and all included libraries and frameworks
 * 
 * @package Music Site
 * @since 1.0.0
 * 
 */

/**
 * Music Site version string
 * 
 * @global string $ms_version
 */

$ms_version = '1.0.0';

/**
 * Music Site database revision number, increments when the database schema changes
 * 
 * @global int $ms_db_version
 */

$ms_db_version = 1;

/** 
 * Holds the required PHP version
 * 
 * @global string $required_php_version
 */

$required_php_version = '7.4.20';

/**
 * Holds the required MySQL version
 * 
 * @global string $required_mysql_version
 */

$required_mysql_version = '15.0';