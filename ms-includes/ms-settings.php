<?php

/**
 * Used to create default settings for the Music Site environment
 * 
 * @package Music Site
 * 
 */

/** 
 * Version info for the current version of the Music Site
 * and all included libraries and frameworks
 * 
 * @global string $ms_version
 * @global int $ms_db_version
 * @global string $required_php_version
 * @global string $required_mysql_version
 */

global $ms_version, $ms_db_version, $required_php_version, $required_mysql_version;

require __DIR__ . '/ms-version.php';